import React from "react";
import styles from "./Search.module.css";

const Search = ({ matchingWord, setMatchingWord }) => {
  return (
    <div className={`${styles.search} flex center border`}>
      <input
        type="text"
        value={matchingWord}
        onChange={(e) => setMatchingWord(e.target.value)}
        placeholder="Pesquise aqui"
      />
      <i onClick={() => {}} className="fas fa-search primaryColor" />
    </div>
  );
};

export default Search;
