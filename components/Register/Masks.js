/* eslint-disable no-useless-escape */
export const telephoneMask = (value) => {
  if (value) {
    return value
      .replace(/\D/g, "")
      .replace(/(\d{2})/, "($1) ")
      .replace(/(\d{5})/, "$1-")
      .replace(/(\d{4})\d+?$/, "$1");
  }
  return undefined;
};

export const emailMask = (value) => {
  if (value) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(value).toLowerCase());
  }
};
