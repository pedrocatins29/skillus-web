import React, { useState, useContext } from "react";
import Skeleton from "react-loading-skeleton";
import Layout from "../../components/Layout/Layout";
import ListProblem from "../../components/Problem/ListProblem";
import FloatingButton from "../../components/Problem/FloatingButton";
import Search from "../../components/Search/Search";
import { useQuery } from "@apollo/react-hooks";
import { Problems } from "../../graphql/problem";
import { withApollo } from "../../lib/apollo";
import { UserContext } from "../../context/userContext";
import Error from "../../components/Error/Error";

const ProblemPage = () => {
  const { loading, error, data } = useQuery(Problems);
  const [searchWord, setSearchWord] = useState("");
  const { user } = useContext(UserContext);
  const [status, setStatus] = useState("all");
  const searchString = searchWord.trim().toLowerCase();
  RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
  };
  const matchingRegex = new RegExp(RegExp.quote(searchString));

  if (error) {
    return <Error message="🐿 Ocorreu um erro ao listar o Problema :(" />;
  }

  return (
    <main>
      <div className="flex full" style={{ padding: "10px 0px" }}>
        Filtrar por:
        <label onClick={() => setStatus("all")}>
          <input className="radio" type="radio" name="ranking" value="problem" defaultChecked />
          <div className="radioDiv"> Todos </div>
        </label>
        <label onClick={() => setStatus("Solucionado")}>
          <input className="radio" type="radio" name="ranking" value="problem" />
          <div className="radioDiv"> Solucionado </div>
        </label>
        <label onClick={() => setStatus("Aguardando ajuda")}>
          <input className="radio" type="radio" name="ranking" value="problem" />
          <div className="radioDiv"> Aguardando ajuda </div>
        </label>
      </div>
      <Search matchingWord={searchWord} setMatchingWord={setSearchWord} />
      {loading ? (
        <Skeleton style={{ borderRadius: 10, marginBottom: 10 }} count={6} height={86} />
      ) : (
        <>
          {data !== undefined ? (
            <ListProblem
              problemList={data.problems.filter((problem) => {
                if (status === "all") {
                  return problem.name.toLowerCase().match(matchingRegex);
                }
                return problem.status == status && problem.name.toLowerCase().match(matchingRegex);
              })}
            />
          ) : (
            <Error message="Ocorreu um erro ao listar os Problemas :(" />
          )}
        </>
      )}
      {/* onde deveria ficar a verificacao se o user ta logado */}
      <FloatingButton />
    </main>
  );
};

export default withApollo({ ssr: true })(Layout(ProblemPage));
