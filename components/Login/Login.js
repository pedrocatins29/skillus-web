import Link from "next/link";
import Input from "../Input/Input";
import React, { useState, useContext, useEffect, useCallback } from "react";
import { useRouter } from "next/router";
import { useMutation } from "@apollo/react-hooks";
import { loginQuery } from "../../graphql/loginQuery";
import { withApollo } from "../../lib/apollo";
import { setAccessToken } from "../../accessToken";
import { Me } from "../../graphql/me";
import { UserContext } from "../../context/userContext";
import styles from "./Login.module.css";
import ToastContainer from "../Toast/ToastContainer";
import { errorToast } from "../Toast/Toast";

const Login = () => {
  const router = useRouter();
  const { setUser } = useContext(UserContext);

  const [login] = useMutation(loginQuery);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    router.prefetch("/ranking");
  }, []);

  return (
    <div className={`${styles.container} flex rows between`}>
      <ToastContainer />
      <div className={`${styles.loginForm} flex column center`}>
        <h2 className={styles.title}>Seja Bem Vindo!</h2>
        <form
          onSubmit={useCallback(
            async (e) => {
              e.preventDefault();
              try {
                const response = await login({
                  variables: {
                    email,
                    password,
                  },
                  update: (store, { data }) => {
                    if (!data) {
                      return null;
                    }

                    store.writeQuery({
                      query: Me,
                      data: {
                        eu: data.login.user,
                      },
                    });
                  },
                });

                if (response.data.login) {
                  setUser(response.data.login.user.id);
                  setAccessToken(response.data.login.accessToken);
                  router.push("/ranking");
                }
              } catch (error) {
                errorToast("🐿 Erro nas credenciais de login !");
                console.log(error.message.slice(-22));
              }
            },
            [email, password]
          )}>
          <Input
            placeholder="Email"
            icon="fas fa-envelope"
            type="text"
            value={email}
            setValue={setEmail}
          />
          <Input
            placeholder="Senha"
            icon="fas fa-lock"
            type="password"
            value={password}
            setValue={setPassword}
          />
          <button className="colorfulButton" style={{ fontSize: 15, width: "100%" }} type="submit">
            Entrar
          </button>
        </form>
        <div className={`${styles.cadastro} flex column center`}>
          <p>Não possui conta?</p>
          <Link href="/register">
            <p className={`${styles.register} bold pointer`}>Cadastre-se já!</p>
          </Link>
        </div>
      </div>
      <h1 className={styles.textIntroduction}>
        União de <p className={styles.contraste}>habilidades</p> com a agilidade do esquilo.
      </h1>
    </div>
  );
};

export default withApollo({ ssr: true })(Login);
