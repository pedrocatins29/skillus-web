import React from "react";
import style from "./Input.module.css";

const Input = ({
  mask,
  icon,
  placeholder,
  toggleRequiredField,
  required,
  type,
  value,
  setValue,
  specialValidation,
  message,
}) => {
  return (
    <>
      <div className={style.backgroundInput}>
        <i className={icon} />
        <input
          onBlur={required ? toggleRequiredField : null}
          value={value}
          onChange={(e) => (mask ? setValue(mask(e.target.value)) : setValue(e.target.value))}
          className={style.inputText}
          type={type}
          id={placeholder}
          required
        />
        {specialValidation === false && <i className="fas fa-times" />}
        <span className={style.floatinglabel}>{placeholder}</span>
      </div>
      <div
        className="errorMessage"
        id={`${placeholder}_em_`}
        style={{ fontSize: 12, color: "red", display: "none", position: "absolute" }}>
        {message || `${placeholder} deve ser preenchido`}
      </div>
      <div style={{ marginBottom: 25 }} />
    </>
  );
};

export default Input;
