import React from "react";
import Link from "next/link";
import ActiveLink from "../ActiveLink/ActiveLink";
import style from "./Layout.module.css";
import { Me } from "../../graphql/me";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { withApollo } from "../../lib/apollo";
import { logout } from "../../graphql/logout";
import { useRouter } from "next/router";

const TopBar = () => {
  const { loading, data } = useQuery(Me);
  const [Logout] = useMutation(logout);
  const router = useRouter();

  if (loading) {
    return null;
  }

  return (
    <div className={`${style.header}`}>
      <nav className={style.links}>
        <Link href={"/ranking"}>
          <div className={style.logo}>
            <img src="https://i.imgur.com/2cZJOFU.png" />
            <p className={style.skillus}>Skill.us</p>
          </div>
        </Link>
        <div className={style.backgoundItem}>
          <ActiveLink href="/">
            <a className={style.navItem}>Home</a>
          </ActiveLink>
        </div>
        <div className={style.backgoundItem}>
          <ActiveLink href="/ranking">
            <a className={style.navItem}>Ranking</a>
          </ActiveLink>
        </div>
        <div className={style.backgoundItem}>
          <ActiveLink href="/problem">
            <a className={style.navItem}>Problemas</a>
          </ActiveLink>
        </div>
      </nav>
      <div className={style.user}>
        {data ? (
          <div>
            <div className="flex flexEnd column">
              <p>{data.eu.name.split(" ")[0]}</p>
              <p
                className={style.logout}
                onClick={async () => {
                  await Logout();
                  router.reload();
                }}>
                Sair
              </p>
            </div>
            <Link href={`/profile/${data.eu.id}`}>
              <img className={style.userImg} src={data.eu.photo} />
            </Link>
          </div>
        ) : (
          <div className={style.loginOptions}>
            <Link href={"/login"}>
              <p>Entrar</p>
            </Link>
            <Link href={"/register"}>
              <p className="colorfulButton">Cadastrar-se</p>
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};

export default withApollo({ ssr: true })(TopBar);
