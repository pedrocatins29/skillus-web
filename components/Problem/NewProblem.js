import React, { useState } from "react";
import style from "./Problem.module.css";
import SelectSkills from "../Skill/SelectSkill";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { CreateProblem } from "../../graphql/createProblem";
import { Me } from "../../graphql/me";
import { Problems } from "../../graphql/problem";
import { skillsQuery } from "../../graphql/skills";
import { withApollo } from "../../lib/apollo";
import ReactLoading from "react-loading";
import { successToast, errorToast } from "../Toast/Toast";
import Error from "../Error/Error";

const NewProblem = ({ close }) => {
  const { loading, data } = useQuery(Me);
  const { data: dataR, error: errorR, loading: loadingR } = useQuery(skillsQuery);
  const [createProblem] = useMutation(CreateProblem);
  const [assunto, setAssunto] = useState("");
  const [description, setDescription] = useState("");
  const [skills, setSkills] = useState([]);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  if (loading || loadingR) {
    return null;
  }

  if (errorR) {
    return <Error message="🐿 Ocorreu um erro :(" />;
  }

  function confirmClose() {
    const exitConfirm = confirm("Deseja cancelar o problema?\nSuas alterações serão apagadas!");
    if (exitConfirm == true) close(true);
  }

  return (
    <form
      onSubmit={async (e) => {
        e.preventDefault();
        setLoadingSubmit(true);
        try {
          await createProblem({
            variables: {
              name: assunto,
              description,
              createdBy: parseInt(data.eu.id),
              skill: skills,
            },
            update(cache, { data: { createProblem } }) {
              const { problems } = cache.readQuery({ query: Problems });
              cache.writeQuery({
                query: Problems,
                data: { problems: problems.concat([createProblem]) },
              });
            },
          });
          setLoadingSubmit(false);
          // successToast("Problema Criado com sucesso");
          close(true);
        } catch (error) {
          setLoadingSubmit(false);
          errorToast(error);
        }
      }}
      className={`${style.form} flex column`}>
      <div className={style.header}>
        <h3>Novo problema</h3>
      </div>
      <label style={{ marginTop: 10 }}>
        <h3>Assunto</h3>
        <input
          value={assunto}
          onChange={(e) => setAssunto(e.target.value)}
          id="name"
          className={style.inputForm}
        />
      </label>
      <SelectSkills skills={dataR.skills} idSkills={skills} setSkill={setSkills} />
      <label>
        <h3>Descreva seu problema</h3>
        <textarea
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          rows="8"
          className={style.textArea}
        />
      </label>
      {loadingSubmit ? (
        <div className={style.buttons}>
          <div style={{ marginRight: 10 }}>
            <ReactLoading type={"spin"} color={"#f77f5c"} height={30} width={30} />
          </div>
          <div className="disabledButton" onClick={() => confirmClose()}>
            Cancelar
          </div>
          <div className="disabledButton" type="submit">
            Enviar
          </div>
        </div>
      ) : (
        <div className={style.buttons}>
          <div className="borderButton" onClick={() => confirmClose()}>
            Cancelar
          </div>
          <button className="confirmButton" type="submit">
            Enviar
          </button>
        </div>
      )}
    </form>
  );
};

export default withApollo({ ssr: true })(NewProblem);
