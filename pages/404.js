import Error from "../components/Error/Error";
import Layout from "../components/Layout/Layout";
import Router from "next/router";

function Custom404() {
  return (
    <div style={{height: '100vh', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
      <Error message="🐿 Essa pagina não existe ):" />
      <div className="flex justifyCenter">
        <p className="colorfulButton" style={{marginTop: 30, fontSize: 40}} onClick={() => Router.back()}>
          Retornar a navegação
        </p>
      </div>
    </div>
  );
}

export default Layout(Custom404);
