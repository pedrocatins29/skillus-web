import React from "react";
import Router from "next/router";
import style from "./Ranking.module.css";

const ListUserSkill = ({ users }) => {
  return (
    <div className={style.listUsers}>
      {users.map((user, index) => (
        <div
          className={`${style.userInfo} cardBorder flex center pointer`}
          key={user.id}
          onClick={() => Router.push(`/profile/${user.id}`)}>
          <div className={style.ranking}>{index + 1}°</div>
          <div className={`${style.user} flex row center`}>
            <img src={user.photo} />
            <div style={{ marginLeft: 10 }}>
              <div className={style.name}>{user.name}</div>
            </div>
          </div>
          <div className={`${style.description} textJustify`}>{user.description}</div>
          <div className={style.skill}>
            <span className={style.skillContainer}>
              <div>
                <i className="fas fa-star" />
                <p className={style.total}>{user.skill[0]?.rating}</p>
              </div>
              <div>
                <p className="skillTag">{user.skill[0]?.name}</p>
              </div>
            </span>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ListUserSkill;
