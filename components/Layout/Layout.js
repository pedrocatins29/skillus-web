/* eslint-disable react/display-name */
import React, { Component } from "react";
import Topbar from "./TopBar";
import BottomBar from "./BottomBar";
import Head from "next/head";
import Router from "next/router";
import NProgress from "nprogress";

Router.events.on("routeChangeStart", () => {
  NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

const Layout = (Page) => {
  return class extends Component {
    render() {
      return (
        <div>
          <Head>
            <title>Skill.us</title>
            <link rel="shortcut icon" href="/assets/favicon.png" />
            <meta property="og:title" content="Skill.us" key="title" />
          </Head>
          <Topbar />
          <Page />
          {Page.name == "Profile" ? <div className="gradientBackground" /> : null}
          {Page.name == "LoginScreen" || Page.name == "HomePage" || Page.name == "Custom404" ? <BottomBar /> : null}
        </div>
      );
    }
  };
};

export default Layout;
