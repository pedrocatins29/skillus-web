import React from "react";
import Layout from "../components/Layout/Layout";
import Register from "../components/Register/Register";

const RegisterSteps = () => {
  return (
    <div className="gradientBackground">
      <main style={{ paddingTop: 25 }}>
        <Register />
      </main>
    </div>
  );
};

export default Layout(RegisterSteps);
