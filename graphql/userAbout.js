import gql from "graphql-tag";

export const userAbout = gql`
  query userAbout($id: ID!) {
    user(id: $id) {
      id
      name
      email
      telephone
      media
      photo
      description
      contact {
        id
        name
        value
      }
      skill {
        id
        name
        rating
      }
    }
  }
`;
