import React from "react";
import style from "./Layout.module.css";

const BottomBar = () => {
  return (
    <div className={style.bottom}>
      <a
        rel="noreferrer"
        target="_blank"
        href="https://gitlab.com/BDAg"
        title="Repositório&nbsp;de&nbsp;alunos">
        <p>Fatec Pompeia - Shunji Nishimura</p>
      </a>
      <div>
        <p>Parceiros:</p>
        <a
          rel="noreferrer"
          target="_blank"
          href="https://www.behance.net/vinesilva"
          title="Vinicius&nbsp;Silva">
          <i className="fab fa-behance" />
        </a>
        <p>Repositórios:</p>
        <a
          rel="noreferrer"
          target="_blank"
          href="https://gitlab.com/BDAg/skill.us/skillus-web"
          title="Web">
          <i className="fab fa-gitlab" />
        </a>
        <a
          rel="noreferrer"
          target="_blank"
          href="https://github.com/pedrocatins29/skillus-server"
          title="Api">
          <i className="fab fa-github" />
        </a>
      </div>
    </div>
  );
};

export default BottomBar;
