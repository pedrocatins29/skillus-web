import React, { useContext } from "react";
import style from "./Home.module.css";
import Link from "next/link";
import { UserContext } from "../../context/userContext";

const Home = () => {
  const { user } = useContext(UserContext);
  return (
    <div className={style.containerImage}>
      <img className={style.backgroundImage} src="img/working.jpg" />
      <h1 className={style.textIntroduction}>
        Quem você pode <span className={style.contraste}>ajudar</span> hoje?
        <br />
        <Link href={user ? "/problem" : "login"}>
          <span className={style.start}>
            Comece <span style={{ color: "#f77f5c" }}>agora</span>!
          </span>
        </Link>
      </h1>
    </div>
  );
};

export default Home;
