import gql from "graphql-tag";

export const loginQuery = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      accessToken
      user {
        id
        name
        photo
      }
    }
  }
`;
