import gql from "graphql-tag";

export const CreateProblem = gql`
  mutation CreateProblem($name: String, $description: String, $createdBy: Int, $skill: [Int]) {
    createProblem(name: $name, description: $description, createdBy: $createdBy, skill: $skill) {
      id
      name
      description
      status
      color
      icon
      skill {
        id
        name
      }
      creator {
        id
        photo
        name
      }
    }
  }
`;
