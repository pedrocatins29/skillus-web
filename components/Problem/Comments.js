import React, { useState } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import Skeleton from "react-loading-skeleton";
import { withApollo } from "../../lib/apollo";
import { AddProblemComment, comments } from "../../graphql/comment";
import style from "./Problem.module.css";
import { useRouter } from "next/dist/client/router";
import ReactLoading from "react-loading";
import Error from "../Error/Error";

const CommentList = ({ problemId, creator, helper, status, user }) => {
  const router = useRouter();
  const [comment, setComment] = useState("");
  const [loadingMutation, setloadingMutation] = useState(false);
  const [AddComment] = useMutation(AddProblemComment);
  const { loading, error, data } = useQuery(comments, {
    variables: {
      id: problemId,
    },
  });

  if (loading) {
    return (
      <div className="full right" style={{ transform: "rotateY(180deg)" }}>
        <Skeleton
          style={{ borderRadius: 10, marginBottom: 10, marginRight: 10 }}
          count={4}
          height={60}
          width={"85%"}
        />
      </div>
    );
  }

  if (error) {
    return <Error message="🐿 Ocorreu um erro ao listar os comentarios :(" />;
  }

  return (
    <>
      {data.comments.map((comment) => (
        <div key={comment.id} className={`${style.comment} cardSimple flex left`}>
          <img className={style.photoComment} src={comment.sender.photo} />
          <div style={{ fontSize: 14 }}>
            <p className="bold">{comment.sender.name}</p>
            <p className="textJustify">{comment.text}</p>
          </div>
        </div>
      ))}
      {data.problem.rating !== null ? (
        <div
          className={`${style.comment} cardSimple flex left`}
          style={{ borderColor: "#f77f5c", borderWidth: 0.5, borderStyle: "solid" }}>
          <img className={style.photoComment} src={creator.photo} />
          <div style={{ fontSize: 14 }}>
            <p className="bold">{creator.name}</p>
            <p className="textJustify">{data.problem.rating.comment}</p>
          </div>
          <p className={`${style.content} primaryColor bold left flex center`}>
            <i className="fas fa-star starsColor " /> {data.problem.rating.note}
          </p>
        </div>
      ) : null}
      {status === "Sendo resolvido" && (user === helper?.name || user === creator.name) ? (
        <div className={`${style.comment} cardSimple flex left`}>
          {user === helper?.name ? (
            <img className={style.photoComment} src={helper.photo} />
          ) : (
            <img className={style.photoComment} src={creator.photo} />
          )}
          <form className="flex center full" style={{ fontSize: 14 }}>
            <input
              value={comment}
              onChange={(e) => setComment(e.target.value)}
              className="textJustify full"
              style={{ marginRight: 10 }}
              placeholder="Escreva um comentário"
            />
            {loadingMutation ? (
              <ReactLoading
                style={{ marginRight: 10 }}
                type={"spin"}
                color={"#f77f5c"}
                height={30}
                width={30}
              />
            ) : (
              <i
                className={`${style.sendButton} fas fa-paper-plane`}
                onClick={async () => {
                  if (comment != "") {
                    try {
                      setloadingMutation(true);
                      await AddComment({
                        variables: {
                          problemId: problemId,
                          text: comment,
                          userIdSender: user === helper?.name ? helper?.id : creator.id,
                        },
                      });
                      setloadingMutation(false);
                      router.reload();
                    } catch (error) {
                      setloadingMutation(false);
                    }
                  } else {
                    return null;
                  }
                }}
              />
            )}
          </form>
        </div>
      ) : null}
    </>
  );
};

export default withApollo({ ssr: true })(CommentList);
