import React from "react";
import Router from "next/router";
import style from "./Ranking.module.css";

const ListUsers = ({ users }) => {
  return (
    <div className={style.listUsers}>
      {users.map((user, index) => (
        <div
          className={`${style.userInfo} cardBorder flex center pointer`}
          key={user.id}
          onClick={() => Router.push(`/profile/${user.id}`)}>
          <div className={style.ranking}>{index + 1}°</div>
          <div className={`${style.user} flex row center`}>
            <img src={user.photo} />
            <div style={{ marginLeft: 10 }}>
              <div className={style.name}>{user.name}</div>
            </div>
          </div>
          <div className={style.description}>{user.description}</div>
          <div className={style.skill}>
            <div>
              <i className={`fas fa-star`} />
              <div className={style.total}>{user.media}</div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ListUsers;
