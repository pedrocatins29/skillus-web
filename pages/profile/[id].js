import React from "react";
import { useRouter } from "next/router";
import Layout from "../../components/Layout/Layout";
import About from "../../components/Profile/About";
import { userAbout } from "../../graphql/userAbout";
import { withApollo } from "../../lib/apollo";
import { useQuery } from "@apollo/react-hooks";
import Error from "../../components/Error/Error";

const Profile = () => {
  const router = useRouter();
  const { loading, error, data } = useQuery(userAbout, {
    variables: {
      id: router.query.id,
    },
  });

  if (loading) {
    return null;
  }

  if (error) {
    return <Error message="🐿 Ocorreu um erro ao listar o Problema :(" />;
  }

  return (
    <main>
      <div className="componentsUser">
        <About userData={data} />
      </div>
    </main>
  );
};

export default withApollo({ ssr: true })(Layout(Profile));
