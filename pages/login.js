import React from "react";
import Layout from "../components/Layout/Layout";
import Login from "../components/Login/Login";

const LoginScreen = () => {
  return (
    <div className="gradientBackground">
      <main style={{ paddingTop: 0 }}>
        <Login />
      </main>
    </div>
  );
};

export default Layout(LoginScreen);
