import Layout from "../components/Layout/Layout";
import Home from "../components/Home/Home";
import { useContext } from "react";
import { IsLoggedIn } from "../context/isLoggedIn";

const HomePage = () => {
  const { logged, setLogged } = useContext(IsLoggedIn);
  console.log(logged);
  return <Home />;
};

export default Layout(HomePage);
