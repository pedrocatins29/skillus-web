/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import Skeleton from "react-loading-skeleton";
import Layout from "../components/Layout/Layout";
import ListUsers from "../components/Ranking/ListUsers";
import ListUserSkill from "../components/Ranking/ListUserSkill";
import Search from "../components/Search/Search";
import { withApollo } from "../lib/apollo";
import { listUsers } from "../graphql/users";
import { useQuery } from "@apollo/react-hooks";

const Ranking = () => {
  const { loading, error, data } = useQuery(listUsers);
  const [filter, setFilter] = useState("users");
  const [searchWord, setSearchWord] = useState("");

  if (loading) {
    return null;
  }

  const searchString = searchWord.trim().toLowerCase();

  RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
  };

  const matchingRegex = new RegExp(RegExp.quote(searchString));

  const filteredArrayBySkill = data.users.filter((user) => {
    const skillFiltered = user.skill.filter((skill) => {
      return skill.name.toLowerCase().match(matchingRegex);
    });
    if (skillFiltered.length > 0) {
      return skillFiltered;
    }
  });

  return (
    <main>
      <div className="flex between full" style={{ padding: "10px 0px" }}>
        <div className="flex">
          Pesquisar por:
          <label onChange={() => setFilter("users")}>
            <input className="radio" type="radio" name="ranking" value="users" defaultChecked />
            <div className="radioDiv"> Pessoas </div>
          </label>
          <label onChange={() => setFilter("skills")}>
            <input className="radio" type="radio" name="ranking" value="skills" />
            <div className="radioDiv"> Habilidades </div>
          </label>
        </div>
      </div>
      <div className="aa">
        <Search matchingWord={searchWord} setMatchingWord={setSearchWord} />
        {loading ? (
          <Skeleton style={{ borderRadius: 10, marginBottom: 10 }} count={6} height={106} />
        ) : (
          <>
            {filter === "users" ? (
              <ListUsers
                users={data.users.filter((user) => {
                  return user.name.toLowerCase().match(matchingRegex);
                })}
              />
            ) : (
              <ListUserSkill users={filteredArrayBySkill} />
            )}
          </>
        )}
      </div>
    </main>
  );
};

export default withApollo({ ssr: true })(Layout(Ranking));
