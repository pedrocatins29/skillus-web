import React, { useState } from "react";
import style from "./Problem.module.css";
import NewProblem from "./NewProblem";
import ToastyContainer from "../Toast/ToastContainer";

const FloatingButton = () => {
  const [modal, setProblem] = useState(true);

  return (
    <div className={`${modal ? style.floatingButton : style.floatingCard}`}>
      <ToastyContainer />
      {modal ? (
        <div className={style.pencil} onClick={() => setProblem(false)}>
          <i className="fas fa-pencil-alt" />
        </div>
      ) : (
        <NewProblem close={setProblem} />
      )}
    </div>
  );
};

export default FloatingButton;
